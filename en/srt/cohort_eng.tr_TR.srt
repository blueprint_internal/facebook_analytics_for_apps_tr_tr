﻿1
00:00:06,522 --> 00:00:08,688
Uygulamalar için Facebook Analytics'te

2
00:00:08,688 --> 00:00:10,363
kohortların nasıl oluşturulduğuna bakacağız.

3
00:00:10,363 --> 00:00:14,488
İlk olarak sol menüdeki Hareketler kısmına tıklayın.

4
00:00:15,488 --> 00:00:18,276
Sonra Kohortlar'a tıklayın.

5
00:00:20,132 --> 00:00:27,621
Sağ üst köşede Kohort Oluştur'a tıklayın.

6
00:00:27,621 --> 00:00:32,277
Şimdi kohortun türünü belirten bir kohort adı girin.

7
00:00:32,277 --> 00:00:34,085
Burada "Install to Purchase" adında

8
00:00:34,085 --> 00:00:37,770
bir kohort oluşturacağız.

9
00:00:39,650 --> 00:00:42,994
"Şu Kişileri Göster:" kısmında

10
00:00:42,994 --> 00:00:45,121
insanların attığı ilk adım olan

11
00:00:45,121 --> 00:00:48,462
belirli bir uygulama olayı seçeceksiniz.

12
00:00:48,462 --> 00:00:50,800
Burada Uygulama Yüklemesi'ni seçiyoruz.

13
00:00:50,800 --> 00:00:54,400
"Ve Daha Sonra" bölümünde ise

14
00:00:54,400 --> 00:00:57,650
bir sonraki uygulama olayımız olan "Alışveriş" olayını seçiyoruz.

15
00:01:06,570 --> 00:01:08,170
Son olarak Kaydet'e tıklıyoruz.

16
00:01:13,093 --> 00:01:17,140
Şimdi bize her bir günde alışveriş yapan insanların

17
00:01:17,140 --> 00:01:19,729
yüzdesini gösteren kohort verilerini görebiliriz.

18
00:01:19,729 --> 00:01:24,103
Bu alışveriş olayının ilişkili bir değeri olduğu için

19
00:01:24,103 --> 00:01:27,950
"Grafik Türü" alanını "Kullanıcı Hareketleri" yerine

20
00:01:27,950 --> 00:01:29,995
"Kümülatif Değerler" yapabiliriz.

21
00:01:29,995 --> 00:01:37,003
Aşağı doğru inerseniz yüzde değerlerini de görebilirsiniz.

22
00:01:37,003 --> 00:01:41,284
"Grafik Türü" alanını "Kullanıcı Hareketleri" yerine "Kümülatif Değer"

23
00:01:41,284 --> 00:01:46,073
yaptığımda zaman içinde kullanıcı başına harcama tutarını görebilirim.

24
00:01:46,073 --> 00:01:48,616
Bu bana zaman içinde insanların uygulamanızda

25
00:01:48,616 --> 00:01:52,064
yaptığı harcama tutarının arttığını söylüyor.

26
00:01:52,064 --> 00:01:55,779
Bu bilgiyi toplam değer gibi şeyleri anlamak için kullanabilirsiniz.

27
00:01:55,779 --> 00:01:59,499
Aşağı inerseniz gün bazında verileri de görebilirsiniz.

28
00:01:59,499 --> 00:02:02,913
Belirli bir günde uygulamanızı yükleyen kişiler,

29
00:02:02,913 --> 00:02:06,340
birkaç gün sonra geri gelip daha fazla

30
00:02:06,340 --> 00:02:08,356
para harcayan kişi sayısı gibi.

31
00:02:08,356 --> 00:02:12,176
İlave gelir elde etmek üzere uygulamanızı optimize etmek için de bundan faydalanabilirsiniz.

