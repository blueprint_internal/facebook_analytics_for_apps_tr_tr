﻿1
00:00:05,513 --> 00:00:08,366
Uygulamalar için Facebook Analytics'te bir huni oluşturmak için

2
00:00:08,366 --> 00:00:12,040
Hareketler kısmında Huniler'e tıklayabilirsiniz.

3
00:00:13,280 --> 00:00:15,391
Ardından Huni Oluştur'a tıklayın.

4
00:00:18,115 --> 00:00:23,303
Huni Adımı Ekle'ye tıklayarak huninize adımlar ekleyebilirsiniz.

5
00:00:23,303 --> 00:00:26,198
Burada tipik bir ticari akışı temsil eden

6
00:00:26,198 --> 00:00:27,707
bir huni oluşturacağız.

7
00:00:27,707 --> 00:00:32,405
Huni Adımı Ekle'ye tıklıyorum ve "Arama" adımını ekliyorum.

8
00:00:35,227 --> 00:00:39,102
Şimdi de "Sepete Ekleme" adımını seçeceğim.

9
00:00:39,102 --> 00:00:43,577
Huninin son adımı olarak da "Alışveriş" adımını ekleyeceğim.

10
00:00:43,577 --> 00:00:47,217
Bu adımların bazılarını daraltmak için

11
00:00:47,217 --> 00:00:50,437
Parametre Seç'e tıklayabilir ve örneğin

12
00:00:50,437 --> 00:00:56,230
50 $ üzeri alışveriş olayları gibi bir seçim yapabilirsiniz.

13
00:00:56,230 --> 00:00:58,275
Bunu yaptıktan sonra Daralt'a tekrar tıklayarak

14
00:00:58,275 --> 00:01:00,502
bölmeyi kapatabilirim.

15
00:01:00,502 --> 00:01:02,998
Hunimi oluşturmak için Uygula'ya tıklayacağım.

16
00:01:12,272 --> 00:01:15,674
Huni oluşturulduktan sonra huninin adımları

17
00:01:15,674 --> 00:01:17,211
arasındaki düşüşü inceleyebilirsiniz.

18
00:01:17,211 --> 00:01:20,218
Aşağı inerek dönüşüm oranlarının sayısal

19
00:01:20,218 --> 00:01:23,045
değerlerini ve huninin her bir adımından

20
00:01:23,045 --> 00:01:25,616
geçen kişi sayısını görebilirsiniz.

21
00:01:25,616 --> 00:01:29,216
Ayrıca huniyi farklı özelliklere göre görüntüleyebilirsiniz.

22
00:01:29,216 --> 00:01:31,699
Yaş veya cinsiyet gibi özelliklere göre kırılımlandırıp

23
00:01:31,699 --> 00:01:34,605
bunların her biri için farklı dönüşüm oranlarını görebilirsiniz.

24
00:01:34,605 --> 00:01:36,764
Segment açılır menüsünü seçerek

25
00:01:36,764 --> 00:01:39,277
huninize bir segment uygulayabilirsiniz.

26
00:01:39,277 --> 00:01:43,156
Huniyi kaydetmek isterseniz Adımı Kaydet'e tıklayıp

27
00:01:47,000 --> 00:01:49,671
bir ad girdikten sonra Farklı Kaydet'e tıklayabilirsiniz.

28
00:01:51,402 --> 00:01:54,696
Uygulamalar için Facebook Analytics'te huniler bu şekilde oluşturulur.

