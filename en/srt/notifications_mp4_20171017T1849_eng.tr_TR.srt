﻿1
00:00:02,041 --> 00:00:03,803
Uygulamalar İçin Facebook Analytics

2
00:00:03,803 --> 00:00:05,788
[ÜCRETSİZ BİLDİRİMLER]
artık istediğiniz kişilere, kendileri için

3
00:00:05,788 --> 00:00:08,094
önemli olan içerik ve tekliflerle erişmeniz için

4
00:00:08,094 --> 00:00:10,120
ücretsiz bildirimler sunuyor.

5
00:00:10,120 --> 00:00:11,886
[UYGULAMADAKİ OLAYLARA DAYALI OLARAK]
İnsanların uygulamanızda gerçekleştirdiği eylemlere

6
00:00:11,886 --> 00:00:13,977
dayalı olarak kampanyalar oluşturmak son derece kolaydır.

7
00:00:15,439 --> 00:00:17,469
Bir kişi sepetindeki ürünü satın almadan bırakmış

8
00:00:17,469 --> 00:00:19,369
[SEPETİNİZDEKİ UÇUŞTA %25 İNDİRİM!]
veya uygulamanızı bir süredir kullanmamışsa,

9
00:00:19,369 --> 00:00:21,752
artık kendisine anında ilet bildirimi gönderebilirsiniz.

10
00:00:21,752 --> 00:00:22,819
[SÜREKLİ KAMPANYALAR]

11
00:00:22,819 --> 00:00:24,136
[ROCKY DAĞLARINDA KAYAK YAPIN!]
Kurallarınızı oluşturduktan sonra

12
00:00:24,136 --> 00:00:26,321
kampanyanız sürekli olarak yayınlanır.

13
00:00:27,646 --> 00:00:29,898
[KOSTA RİKA UÇUŞLARINDA %50 İNDİRİM]

14
00:00:29,898 --> 00:00:31,759
İlgi çekici deneyimler yaratın ve

15
00:00:31,759 --> 00:00:33,763
[ÖZEL TEKLİFLER OLUŞTURUN]
insanları özel teklifleriniz, yeni ürünleriniz

16
00:00:33,763 --> 00:00:35,092
ve daha fazlası hakkında bilgilendirmek için

17
00:00:35,092 --> 00:00:39,748
[ÖZELLEŞTİRİLEBİLİR TASARIMLAR]
özelleştirilebilir uygulama içi bildirimleri kullanın.

18
00:00:39,748 --> 00:00:44,025
Uygulamalar İçin Facebook Analytics artık bildirim gönderme özelliği sunuyor.

19
00:00:44,025 --> 00:00:45,598
[Pazarlamanızı otomatik hale getirin]
Pazarlamanızı otomatik hale getirin.

20
00:00:45,598 --> 00:00:49,162
[Uygulamalar İçin Facebook Analytics]

