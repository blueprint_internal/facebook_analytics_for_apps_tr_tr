﻿1
00:00:05,766 --> 00:00:10,155
Uygulamalar için Facebook Analytics'te çeşitli raporlarda

2
00:00:10,155 --> 00:00:12,178
segment oluşturmak son derece kolaydır.

3
00:00:12,178 --> 00:00:15,229
Size gelir raporunda nasıl segment oluşturacağınızı

4
00:00:15,229 --> 00:00:16,817
adım adım göstereceğim.

5
00:00:18,647 --> 00:00:22,181
Gelir raporuna tıkladım ve şu anda raporda

6
00:00:22,181 --> 00:00:25,518
herhangi bir filtre bulunmadığını görüyorum.

7
00:00:25,518 --> 00:00:28,048
Rapor, kullanıcıların %100'ünü gösteriyor.

8
00:00:28,048 --> 00:00:32,317
Ancak Düzenle'ye tıklayarak kolayca bir segment uygulayabilirim.

9
00:00:32,317 --> 00:00:36,481
Düzenle'ye tıkladığımda bir koşul seçebilirim.

10
00:00:36,481 --> 00:00:40,946
Olay, Demografik Bilgiler, Cihaz Bilgisi veya Yükleme Kaynağı

11
00:00:40,946 --> 00:00:43,346
gibi farklı türde koşullar arasından seçim yapabilirim.

12
00:00:43,346 --> 00:00:47,118
Ayrıca istersem belirli bir eylemi en çok gerçekleştiren kişilere

13
00:00:47,118 --> 00:00:51,343
göre filtreleme yapmak için bir Yüzdelik Koşulu ekleyebilirim.

14
00:00:51,343 --> 00:00:54,601
Bu örnekte Demografik Bilgilere dayalı olarak bir

15
00:00:54,601 --> 00:00:57,339
koşul oluşturacağım.

16
00:00:57,339 --> 00:00:59,287
Demografik Bilgiler'e tıklıyorum.

17
00:00:59,287 --> 00:01:02,956
"Ülke" parametresini seçeceğim.

18
00:01:02,956 --> 00:01:07,488
Ülke alanına tıklayıp

19
00:01:07,488 --> 00:01:09,427
Amerika Birleşik Devletleri'ni seçeceğim.

20
00:01:09,427 --> 00:01:12,759
Ardından Yüzdelik Ekle'ye tıklayacağım.

21
00:01:12,759 --> 00:01:17,011
Tıkladıktan sonra, yüzdeliğimin temel alacağı olayı seçeceğim.

22
00:01:17,011 --> 00:01:24,621
Uygulamayı başlatan ve bu eylemi en çok gerçekleştiren %10'luk dilimi seçelim.

23
00:01:27,545 --> 00:01:30,014
Tamam'a tıklıyorum.

24
00:01:30,014 --> 00:01:33,492
Şimdi, grafiğin güncellendiğini ve sadece bu segmente

25
00:01:33,492 --> 00:01:36,411
ait verileri gösterdiğini görebilirsiniz.

26
00:01:36,411 --> 00:01:39,879
Bu segment henüz kayıtlı değil.

27
00:01:39,879 --> 00:01:44,139
İstersem Diğer'e ve Farklı Kaydet'e tıklayabilirim.

28
00:01:44,139 --> 00:01:45,970
Segmentime bir ad veriyorum ve

29
00:01:50,138 --> 00:01:51,403
Farklı Kaydet'e tıklıyorum.

30
00:01:54,232 --> 00:01:58,982
Herhangi bir zamanda segmente geri dönmek veya

31
00:01:58,982 --> 00:02:01,850
segmenti gizlemek istersem, Gizle'ye tıklayabilir ya da

32
00:02:01,850 --> 00:02:05,998
Düzenle'ye tıklayıp segment açılır menüsünden başka segmentler seçebilirim.

33
00:02:05,998 --> 00:02:08,939
Uygulamalar için Analytics'te segmentler bu şekilde oluşturulur.

