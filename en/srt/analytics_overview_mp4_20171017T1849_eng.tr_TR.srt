﻿1
00:00:00,456 --> 00:00:02,769
Harika bir uygulama oluşturmak oldukça karmaşık bir iştir.

2
00:00:02,769 --> 00:00:06,508
Tasarım, optimizasyon, pazarlama... Tüm bunların arasında kaybolmak çok kolaydır.

3
00:00:06,508 --> 00:00:07,431
Ama iyi haberlerimiz var:

4
00:00:07,431 --> 00:00:10,856
Uygulamalar İçin Facebook Analytics daha akıllı kararlar almanıza yardımcı oluyor.

5
00:00:10,856 --> 00:00:13,758
Her gün Facebook'u kullanan 1,2 milyarı aşkın kişi sayesinde

6
00:00:13,758 --> 00:00:16,202
toplu hedef kitle istatistiklerine erişebilirsiniz;

7
00:00:16,202 --> 00:00:18,650
örneğin yaş, cinsiyet, eğitim, ilgi alanları,

8
00:00:18,650 --> 00:00:20,420
ülke, dil ve çok daha fazlası.

9
00:00:20,420 --> 00:00:24,169
[DETAYLI HEDEF KİTLE İSTATİSTİKLERİNE ERİŞİN]
Sonunda uygulamanızı kullanan insanları anlayabileceksiniz.

10
00:00:24,169 --> 00:00:26,898
Eğilimleri inceleyin ve dönüşüm, kullanıcı tutma oranı,

11
00:00:26,898 --> 00:00:29,693
ve toplam değer için optimizasyon yapın.

12
00:00:29,693 --> 00:00:32,090
[ADRESE TESLİM MARKET ÜRÜNLERİ, ŞİMDİ %15 İNDİRİMLE SİPARİŞ VERİN!]
Müşterilerinize erişmek ve onlarla tekrar etkileşim kurmak için

13
00:00:32,090 --> 00:00:35,768
[BİLDİRİMLERİ KULLANARAK MÜŞTERİLERİNİZLE YENİDEN ETKİLEŞİM KURUN]
anında ilet bildirimlerini ve uygulama içi bildirimleri kullanın.

14
00:00:35,768 --> 00:00:38,000
En iyisi de, bildirimleri kullanmak

15
00:00:38,000 --> 00:00:40,904
[ÜCRETSİZ]
kolay, ücretsiz ve Facebook girişi gerektirmiyor.

16
00:00:45,021 --> 00:00:46,579
[Daha akıllı kararlar verin]
Uygulamalar için Facebook Analytics ile

17
00:00:46,579 --> 00:00:48,355
[Uygulamalar İçin Facebook Analytics] daha akıllı kararlar verin.

