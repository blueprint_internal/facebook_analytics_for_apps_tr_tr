﻿1
00:00:00,000 --> 00:00:04,879
[Uygulamalar İçin Facebook Analytics
Özel Kırılım Oluşturma]

2
00:00:05,976 --> 00:00:08,978
Özel kırılımların nasıl oluşturulduğunu size adım adım göstereceğiz.

3
00:00:08,978 --> 00:00:12,678
Özel kırılım oluşturmak için ilk olarak Hareketler'e tıklayın

4
00:00:12,678 --> 00:00:15,451
ve Kırılımlar'ı seçin.

5
00:00:15,451 --> 00:00:20,050
Ardından sağ üst köşede Yeni Oluştur'a tıklayın.

6
00:00:20,050 --> 00:00:22,877
Şimdi kırılımınıza bir ad vermeniz gerekir.

7
00:00:22,877 --> 00:00:26,234
Olay adını ve kırılım için kullandığınız parametreleri

8
00:00:26,234 --> 00:00:28,524
içeren bir ad seçebilirsiniz.

9
00:00:28,524 --> 00:00:35,958
Burada Ülkeye, Şehre ve Platforma

10
00:00:35,958 --> 00:00:38,294
Göre Arama adını veriyoruz.

11
00:00:39,830 --> 00:00:42,154
Şimdi arama olaylarımızı seçeceğiz.

12
00:00:44,378 --> 00:00:46,885
Bu açılır menülerin her birinde, kırılımını göreceğiniz

13
00:00:46,885 --> 00:00:49,356
farklı bir parametre seçebilirsiniz.

14
00:00:49,356 --> 00:00:54,736
Burada Ülke ve Şehir seçeneklerini ekliyoruz

15
00:00:54,736 --> 00:00:58,319
ve ardından Platform seçeneğini ekliyoruz.

16
00:00:58,319 --> 00:01:01,035
Son olarak, Kaydet'e tıklamanız gerekiyor.

17
00:01:04,778 --> 00:01:07,675
Kırılım tablonuz kısa süre içinde oluşturulacaktır.

18
00:01:07,675 --> 00:01:10,613
Kırılım tablosunda ilk sıradaki ülkeyi,

19
00:01:10,613 --> 00:01:12,114
şehri, platformu ve ilgili olayın

20
00:01:12,114 --> 00:01:14,485
toplam sayısını görebilirsiniz.

21
00:01:14,485 --> 00:01:18,194
Alışveriş olayında olduğu gibi, ilişkili bir değer varsa

22
00:01:18,194 --> 00:01:21,107
değeri ve tekil kullanıcıları görebilirsiniz.

23
00:01:21,107 --> 00:01:23,107
Gördüğünüz sütunları özelleştirmek için

24
00:01:23,107 --> 00:01:25,928
buradaki çark simgesine tıklayabilirsiniz

25
00:01:25,928 --> 00:01:30,227
ve bu açılır menüye tıklayarak bir segment uygulayabilirsiniz.

26
00:01:30,227 --> 00:01:31,927
Bu şekilde bir kırılım oluşturmuş olursunuz.

