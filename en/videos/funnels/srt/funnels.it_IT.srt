﻿1
00:00:05,000 --> 00:00:09,000
Puoi creare un funnel in Facebook Analytics per le app

2
00:00:09,000 --> 00:00:13,000
cliccando su "Funnel" nella sezione "Attività".

3
00:00:13,000 --> 00:00:17,000
A questo punto, clicca su "Crea funnel".

4
00:00:17,000 --> 00:00:21,000
Puoi aggiungere fasi al tuo funnel

5
00:00:21,000 --> 00:00:25,000
cliccando su "Aggiungi fase del funnel". In questo esempio, creeremo un funnel

6
00:00:25,000 --> 00:00:29,000
che rappresenta un tipico flusso di acquisto. Clicco su 

7
00:00:29,000 --> 00:00:33,000
"Aggiungi fase del funnel" e digito "Ricerca".

8
00:00:33,000 --> 00:00:37,000
Aggiungo un'altra fase del funnel,

9
00:00:37,000 --> 00:00:41,000
ovvero "Aggiunta al carrello", quindi aggiungo un'ultima

10
00:00:41,000 --> 00:00:45,000
fase del funnel, ovvero "Acquisto". Tieni presente 

11
00:00:45,000 --> 00:00:49,000
che puoi ottimizzare alcune fasi cliccando su "Seleziona parametro"

12
00:00:49,000 --> 00:00:53,000
e puoi scegliere opzioni come "Azione di acquisto

13
00:00:53,000 --> 00:00:57,000
superiore a 50 $". Una volta

14
00:00:57,000 --> 00:01:01,000
terminato, posso cliccare su "Ottimizza" per chiudere,

15
00:01:01,000 --> 00:01:05,000
quindi su "Applica" per creare il mio funnel.

17
00:01:09,000 --> 00:01:13,000
Dopo aver creato il funnel,

18
00:01:13,000 --> 00:01:17,000
puoi controllare la differenza complessiva tra le fasi.

19
00:01:17,000 --> 00:01:21,000
Puoi scorrere verso il basso per vedere i valori numerici 

20
00:01:21,000 --> 00:01:25,000
per i tassi di conversione, nonché il numero di persone che completeranno ogni fase del funnel.

21
00:01:25,000 --> 00:01:29,000
Puoi anche visualizzare il funnel per attributi diversi.

22
00:01:29,000 --> 00:01:33,000
Scegli ad esempio età e genere e vedi i diversi tassi di conversione.

23
00:01:33,000 --> 00:01:37,000
Puoi applicare un segmento a un funnel selezionando

24
00:01:37,000 --> 00:01:41,000
il menu a discesa dei segmenti. Puoi anche 

25
00:01:41,000 --> 00:01:45,000
salvare il funnel cliccando su "Salva". 

26
00:01:45,000 --> 00:01:49,000
Scegli un nome e clicca su "Salva come".

27
00:01:49,000 --> 00:01:53,000
Ecco come puoi creare un funnel

28
00:01:53,000 --> 00:01:56,900
in Facebook Analytics per le app.