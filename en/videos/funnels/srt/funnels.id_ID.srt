﻿1
00:00:05,000 --> 00:00:09,000
Anda dapat membuat corong di Facebook Analytics untuk Aplikasi dengan

2
00:00:09,000 --> 00:00:13,000
mengeklik "Corong" di bagian "Aktivitas".

3
00:00:13,000 --> 00:00:17,000
Selanjutnya, klik "Buat Corong."

4
00:00:17,000 --> 00:00:21,000
Anda dapat menambahkan langkah-langkah ke corong Anda dengan

5
00:00:21,000 --> 00:00:25,000
mengeklik "Tambahkan Langkah Corong." Dalam hal ini, kita akan membuat

6
00:00:25,000 --> 00:00:29,000
corong yang mewakili alur perdagangan secara umum. Jadi saya akan mengeklik 

7
00:00:29,000 --> 00:00:33,000
"Tambahkan Langkah Corong," selanjutnya saya akan mencari langkah corong.

8
00:00:33,000 --> 00:00:37,000
Saya akan menambahkan langkah corong lainnya,

9
00:00:37,000 --> 00:00:41,000
yaitu "Tambahkan ke Keranjang." Lalu saya akan menambahkan

10
00:00:41,000 --> 00:00:45,000
langkah corong terakhir, yaitu "Pembelian." Sekarang perhatikan: 

11
00:00:45,000 --> 00:00:49,000
Anda sebenarnya dapat mempertajam beberapa langkah tersebut dengan mengeklik "Pilih Parameter,"

12
00:00:49,000 --> 00:00:53,000
dan Anda dapat memilih opsi seperti "Peristiwa

13
00:00:53,000 --> 00:00:57,000
Pembelian Lebih Besar Dari $50." Setelah saya melanjutkan

14
00:00:57,000 --> 00:01:01,000
dan menyelesaikannya, saya dapat mengeklik "Pertajam" untuk menutupnya, lalu

15
00:01:01,000 --> 00:01:05,000
mengeklik "Terapkan" untuk membuat corong saya.

17
00:01:09,000 --> 00:01:13,000
Setelah corong

18
00:01:13,000 --> 00:01:17,000
dibuat, Anda dapat melihat seluruh perbedaan antar langkah.

19
00:01:17,000 --> 00:01:21,000
Anda dapat menggulir ke bawah untuk melihat nilai numerik untuk 

20
00:01:21,000 --> 00:01:25,000
tingkat konversi, serta jumlah orang yang melalui setiap langkah di corong.

21
00:01:25,000 --> 00:01:29,000
Anda juga dapat menampilkan corong dengan berbagai atribusi.

22
00:01:29,000 --> 00:01:33,000
Jadi perinci atribut berdasarkan hal seperti usia atau jenis kelamin, dan lihat tingkat konversi yang berbeda.

23
00:01:33,000 --> 00:01:37,000
Anda dapat menerapkan segmen ke corong Anda dengan memilih

24
00:01:37,000 --> 00:01:41,000
menu pilihan segmen. Anda juga dapat 

25
00:01:41,000 --> 00:01:45,000
menyimpan corong dengan mengeklik "Simpan." 

26
00:01:45,000 --> 00:01:49,000
Beri nama dan klik "Simpan Sebagai."

27
00:01:49,000 --> 00:01:53,000
Dan itulah cara membuat corong

28
00:01:53,000 --> 00:01:56,900
di Facebook Analytics untuk Aplikasi.