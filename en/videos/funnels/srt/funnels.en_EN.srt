﻿1
00:00:05,000 --> 00:00:09,000
You can create a funnel in Facebook Analytics for Apps by

2
00:00:09,000 --> 00:00:13,000
clicking on "Funnels" under the "Activity" section.

3
00:00:13,000 --> 00:00:17,000
Next, click "Create Funnel."

4
00:00:17,000 --> 00:00:21,000
You can add steps to your funnel by

5
00:00:21,000 --> 00:00:25,000
clicking on the "Add Funnel Step." In this case, we're going to create a

6
00:00:25,000 --> 00:00:29,000
funnel that represents a typical commerce flow. So I'll click 

7
00:00:29,000 --> 00:00:33,000
"Add Funnel Step," and then I'll search a funnel step.

8
00:00:33,000 --> 00:00:37,000
I will add another funnel step,

9
00:00:37,000 --> 00:00:41,000
which is "Add to Cart." And then, I'll add a final

10
00:00:41,000 --> 00:00:45,000
funnel step, which is "Purchases." Now note: 

11
00:00:45,000 --> 00:00:49,000
you can actually refine some of these steps by clicking on "Select Parameter,"

12
00:00:49,000 --> 00:00:53,000
and you can choose things such as "Purchase

13
00:00:53,000 --> 00:00:57,000
Event Greater Than $50." Once I've gone

14
00:00:57,000 --> 00:01:01,000
ahead and finished that, I can click "Refine" to close it, and then

15
00:01:01,000 --> 00:01:05,000
click "Apply" to create my funnel.

16
00:01:05,000 --> 00:01:09,000


17
00:01:09,000 --> 00:01:13,000
Once the funnel

18
00:01:13,000 --> 00:01:17,000
is created, you can look at the overall difference between steps.

19
00:01:17,000 --> 00:01:21,000
You can scroll down to see the numerical values for 

20
00:01:21,000 --> 00:01:25,000
the conversion rates, as well as the number of people who make it through each step in the funnel.

21
00:01:25,000 --> 00:01:29,000
You can also show the funnel by different attributes.

22
00:01:29,000 --> 00:01:33,000
So break it down by things like age or gender, and see different conversion rates.

23
00:01:33,000 --> 00:01:37,000
You can apply a segment to your funnel by selecting

24
00:01:37,000 --> 00:01:41,000
the segment dropdown. You can also 

25
00:01:41,000 --> 00:01:45,000
save the funnel by clicking "Save." 

26
00:01:45,000 --> 00:01:49,000
Give a name, and click "Save As."

27
00:01:49,000 --> 00:01:53,000
And that's how you create a funnel

28
00:01:53,000 --> 00:01:56,900
in Facebook Analytics for Apps.

