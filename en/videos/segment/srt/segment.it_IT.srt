﻿1
00:00:05,000 --> 00:00:09,000
È facile configurare un segmento

2
00:00:09,000 --> 00:00:13,000
in Facebook Analytics per le app in diversi report. Illustrerò

3
00:00:13,000 --> 00:00:17,000
come configurare un segmento nel report "Ricavi".

4
00:00:17,000 --> 00:00:21,000
Clicco su "Ricavi" e

5
00:00:21,000 --> 00:00:25,000
posso vedere che al momento il report non ha alcun filtro 

6
00:00:25,000 --> 00:00:29,000
e mostra tutti gli utenti. Tuttavia, posso 

7
00:00:29,000 --> 00:00:33,000
applicare un segmento cliccando su "Modifica".

8
00:00:33,000 --> 00:00:37,000
Dopo aver cliccato su "Modifica", posso cliccare su "Seleziona condizione" e scegliere

9
00:00:37,000 --> 00:00:41,000
tra diversi tipi di condizioni, compresi "Azioni", "Dati demografici", 

10
00:00:41,000 --> 00:00:45,000
"Informazioni sul dispositivo" o "Origine di installazione". Posso anche 

11
00:00:45,000 --> 00:00:49,000
aggiungere una condizione per percentile per filtrare in base

12
00:00:49,000 --> 00:00:53,000
alle persone che eseguono di più una certa azione. In questo esempio, 

13
00:00:53,000 --> 00:00:57,000
creerò una condizione basata sui dati demografici.

14
00:00:57,000 --> 00:01:01,000
Seleziono il parametro 

15
00:01:01,000 --> 00:01:05,000
"Paese". Clicco 

16
00:01:05,000 --> 00:01:09,000
sul campo e seleziono "Stati Uniti d'America".

17
00:01:09,000 --> 00:01:13,000
Clicco quindi su "Aggiungi percentile".

18
00:01:13,000 --> 00:01:17,000
Dopo averlo fatto, seleziono l'azione su cui desidero basare il percentile.

19
00:01:17,000 --> 00:01:21,000
Definiamo le persone che avviano l'app 

20
00:01:21,000 --> 00:01:25,000
e fanno parte del primo 10% delle persone.

21
00:01:25,000 --> 00:01:29,000
A questo punto, clicco su "Applica".

22
00:01:29,000 --> 00:01:33,000
Adesso puoi vedere che il grafico si sta

23
00:01:33,000 --> 00:01:37,000
aggiornando per mostrarmi i dati di questo segmento. 

24
00:01:37,000 --> 00:01:41,000
Questo segmento non è ancora salvato, pertanto posso 

25
00:01:41,000 --> 00:01:45,000
cliccare su "Altro" e selezionare "Salva come". Adesso posso assegnare 

26
00:01:45,000 --> 00:01:49,000
un nome al mio segmento.

27
00:01:49,000 --> 00:01:53,000
Clicco su "Salva come".

28
00:01:53,000 --> 00:01:57,000
Se in un dato momento, voglio 

29
00:01:57,000 --> 00:02:01,000
tornare al segmento o nasconderlo, posso cliccare su "Nascondi"

30
00:02:01,000 --> 00:02:05,000
o "Modifica". Inoltre, posso cliccare sul menu a discesa dei segmenti per selezionarne altri.

31
00:02:05,000 --> 00:02:09,000
Ecco come puoi creare i segmenti in Analytics per le app.